﻿using SharedClasses;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Launcher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (!PreChecks.firstSetupRun())
            {
                DialogResult result = MessageBox.Show("Die HTL-Umgebung von Creo 3.0 wurde noch nicht eingerichtet!\nWollen Sie dies jetzt tun? (Wird zum Start von CREO benötigt)",
                    "Setup erforderlich!",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning);

                switch (result)
                {
                    case DialogResult.Yes:
                        try
                        {
                            Process notePad = new Process();
                            notePad.StartInfo.FileName = "Creo_Configurator.exe";
                            notePad.Start();
                        } catch (Exception ignored)
                        {
                            MessageBox.Show("Der Creo Konfigurator konnte nicht gefunden werden! Bitte installiere das Programm neu...",
                                    "Creo Konfigurator konnte nicht gestartet werden!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                        }
                        break;
                }
            }
            else
            {
                Application.Run(new Launcher());
            }
        }
    }


}
