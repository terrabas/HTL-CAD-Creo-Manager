﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedClasses
{
    public static class PreChecks
    {
        static public bool firstSetupRun()
        {
            bool ret = false;
            try
            {
                ret = Convert.ToBoolean(RegistryHelper.Read("setupRan"));
            } catch (Exception ignored) { }
            return ret;
        }

        static public bool checkCreoPath(string Path)
        {
            return File.Exists(Path + @"\Parametric\bin\parametric.exe");
        }
    }
}
