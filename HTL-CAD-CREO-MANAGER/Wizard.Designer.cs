﻿namespace HTL_CAD_CREO_MANAGER
{
    partial class Wizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.back = new System.Windows.Forms.Button();
            this.forward = new System.Windows.Forms.Button();
            this.IntroPanel = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CreoPathPanel = new System.Windows.Forms.Panel();
            this.showOutlinesCheck = new System.Windows.Forms.CheckBox();
            this.betterGraphicsCheck = new System.Windows.Forms.CheckBox();
            this.gpuCheck = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.creoInstallPathPicker = new System.Windows.Forms.Button();
            this.creoInstallPath = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.HTLStandardsPanel = new System.Windows.Forms.Panel();
            this.htlStandardsPicker = new System.Windows.Forms.ComboBox();
            this.htlStandardsSelect = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.htlStandardsPath = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.IntroPanel.SuspendLayout();
            this.CreoPathPanel.SuspendLayout();
            this.HTLStandardsPanel.SuspendLayout();
            this.htlStandardsSelect.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.back);
            this.panel1.Controls.Add(this.forward);
            this.panel1.Location = new System.Drawing.Point(344, 314);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(155, 24);
            this.panel1.TabIndex = 0;
            // 
            // back
            // 
            this.back.Enabled = false;
            this.back.Location = new System.Drawing.Point(0, 0);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(75, 23);
            this.back.TabIndex = 1;
            this.back.Text = "Zurück";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.backwards_Click);
            // 
            // forward
            // 
            this.forward.Location = new System.Drawing.Point(81, 0);
            this.forward.Name = "forward";
            this.forward.Size = new System.Drawing.Size(75, 23);
            this.forward.TabIndex = 0;
            this.forward.Text = "Weiter";
            this.forward.UseVisualStyleBackColor = true;
            this.forward.Click += new System.EventHandler(this.forward_Click);
            // 
            // IntroPanel
            // 
            this.IntroPanel.Controls.Add(this.label4);
            this.IntroPanel.Controls.Add(this.label3);
            this.IntroPanel.Controls.Add(this.label2);
            this.IntroPanel.Location = new System.Drawing.Point(12, 12);
            this.IntroPanel.Name = "IntroPanel";
            this.IntroPanel.Size = new System.Drawing.Size(487, 277);
            this.IntroPanel.TabIndex = 1;
            this.IntroPanel.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(271, 261);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(213, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "Klicke auf \"Weiter\" um anzufangen.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(356, 32);
            this.label3.TabIndex = 1;
            this.label3.Text = "Dieser Assistent wird dich bei der korrekten Installation der \r\nHTL-Standards unt" +
    "erstützen.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(297, 32);
            this.label2.TabIndex = 0;
            this.label2.Text = "Willkommen beim Einrichtungsassistenten\r\nfür die CREO-HTL-Standards!";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.Location = new System.Drawing.Point(9, 319);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "v0.1 © 2017 Martin Schwarz";
            // 
            // CreoPathPanel
            // 
            this.CreoPathPanel.Controls.Add(this.showOutlinesCheck);
            this.CreoPathPanel.Controls.Add(this.betterGraphicsCheck);
            this.CreoPathPanel.Controls.Add(this.gpuCheck);
            this.CreoPathPanel.Controls.Add(this.label8);
            this.CreoPathPanel.Controls.Add(this.creoInstallPathPicker);
            this.CreoPathPanel.Controls.Add(this.creoInstallPath);
            this.CreoPathPanel.Controls.Add(this.label6);
            this.CreoPathPanel.Controls.Add(this.label7);
            this.CreoPathPanel.Location = new System.Drawing.Point(12, 13);
            this.CreoPathPanel.Name = "CreoPathPanel";
            this.CreoPathPanel.Size = new System.Drawing.Size(487, 277);
            this.CreoPathPanel.TabIndex = 3;
            this.CreoPathPanel.Visible = false;
            // 
            // showOutlinesCheck
            // 
            this.showOutlinesCheck.AutoSize = true;
            this.showOutlinesCheck.Location = new System.Drawing.Point(6, 180);
            this.showOutlinesCheck.Name = "showOutlinesCheck";
            this.showOutlinesCheck.Size = new System.Drawing.Size(298, 17);
            this.showOutlinesCheck.TabIndex = 8;
            this.showOutlinesCheck.Text = "Zeige Objektumrisse (Verbessert Sichtbarkeit von Kanten)";
            this.showOutlinesCheck.UseVisualStyleBackColor = true;
            // 
            // betterGraphicsCheck
            // 
            this.betterGraphicsCheck.AutoSize = true;
            this.betterGraphicsCheck.Enabled = false;
            this.betterGraphicsCheck.Location = new System.Drawing.Point(6, 157);
            this.betterGraphicsCheck.Name = "betterGraphicsCheck";
            this.betterGraphicsCheck.Size = new System.Drawing.Size(288, 17);
            this.betterGraphicsCheck.TabIndex = 7;
            this.betterGraphicsCheck.Text = "Bessere Grafiken (GPU-intensiv bei größeren Projekten)";
            this.betterGraphicsCheck.UseVisualStyleBackColor = true;
            // 
            // gpuCheck
            // 
            this.gpuCheck.AutoSize = true;
            this.gpuCheck.Location = new System.Drawing.Point(6, 134);
            this.gpuCheck.Name = "gpuCheck";
            this.gpuCheck.Size = new System.Drawing.Size(351, 17);
            this.gpuCheck.TabIndex = 6;
            this.gpuCheck.Text = "GPU Beschleunigung (verwendet wenn vorhanden die externe GPU)";
            this.gpuCheck.UseVisualStyleBackColor = true;
            this.gpuCheck.CheckedChanged += new System.EventHandler(this.gpuCheck_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "CREO 3 Installationspfad";
            // 
            // creoInstallPathPicker
            // 
            this.creoInstallPathPicker.Location = new System.Drawing.Point(450, 89);
            this.creoInstallPathPicker.Name = "creoInstallPathPicker";
            this.creoInstallPathPicker.Size = new System.Drawing.Size(33, 20);
            this.creoInstallPathPicker.TabIndex = 4;
            this.creoInstallPathPicker.Text = "...";
            this.creoInstallPathPicker.UseVisualStyleBackColor = true;
            this.creoInstallPathPicker.Click += new System.EventHandler(this.creoInstallPathPicker_Click);
            // 
            // creoInstallPath
            // 
            this.creoInstallPath.Location = new System.Drawing.Point(6, 89);
            this.creoInstallPath.Name = "creoInstallPath";
            this.creoInstallPath.Size = new System.Drawing.Size(438, 20);
            this.creoInstallPath.TabIndex = 3;
            this.creoInstallPath.Text = "C:\\...";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(202, 16);
            this.label6.TabIndex = 1;
            this.label6.Text = "Creo 3 Spezifische Einstellungen";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(162, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "PTC CREO 3 Optionen";
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "Wähle den PTC CREO 3 Installationsordner (z.B. ../PTC/CREO 3.0/M080/).";
            this.folderBrowserDialog1.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // HTLStandardsPanel
            // 
            this.HTLStandardsPanel.Controls.Add(this.panel2);
            this.HTLStandardsPanel.Controls.Add(this.htlStandardsPicker);
            this.HTLStandardsPanel.Controls.Add(this.htlStandardsSelect);
            this.HTLStandardsPanel.Controls.Add(this.label9);
            this.HTLStandardsPanel.Controls.Add(this.label10);
            this.HTLStandardsPanel.Location = new System.Drawing.Point(12, 13);
            this.HTLStandardsPanel.Name = "HTLStandardsPanel";
            this.HTLStandardsPanel.Size = new System.Drawing.Size(487, 277);
            this.HTLStandardsPanel.TabIndex = 9;
            this.HTLStandardsPanel.Visible = false;
            // 
            // htlStandardsPicker
            // 
            this.htlStandardsPicker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.htlStandardsPicker.FormattingEnabled = true;
            this.htlStandardsPicker.Location = new System.Drawing.Point(6, 62);
            this.htlStandardsPicker.Name = "htlStandardsPicker";
            this.htlStandardsPicker.Size = new System.Drawing.Size(226, 21);
            this.htlStandardsPicker.TabIndex = 7;
            this.htlStandardsPicker.SelectedIndexChanged += new System.EventHandler(this.htlStandardsPicker_SelectedIndexChanged);
            // 
            // htlStandardsSelect
            // 
            this.htlStandardsSelect.Controls.Add(this.label5);
            this.htlStandardsSelect.Controls.Add(this.htlStandardsPath);
            this.htlStandardsSelect.Controls.Add(this.button1);
            this.htlStandardsSelect.Location = new System.Drawing.Point(35, 112);
            this.htlStandardsSelect.Name = "htlStandardsSelect";
            this.htlStandardsSelect.Size = new System.Drawing.Size(409, 103);
            this.htlStandardsSelect.TabIndex = 6;
            this.htlStandardsSelect.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(321, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "HTL-Standards Installationspfad (normalerweise der \"PTC\" Ordner)";
            // 
            // htlStandardsPath
            // 
            this.htlStandardsPath.Location = new System.Drawing.Point(6, 28);
            this.htlStandardsPath.Name = "htlStandardsPath";
            this.htlStandardsPath.Size = new System.Drawing.Size(361, 20);
            this.htlStandardsPath.TabIndex = 3;
            this.htlStandardsPath.Text = "C:\\...";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(373, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 20);
            this.button1.TabIndex = 4;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(203, 16);
            this.label9.TabIndex = 1;
            this.label9.Text = "Konfiguration der HTL-Standards";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(159, 16);
            this.label10.TabIndex = 0;
            this.label10.Text = "CREO HTL-Standards";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Location = new System.Drawing.Point(35, 112);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(409, 100);
            this.panel2.TabIndex = 7;
            this.panel2.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 12);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(158, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "HTL-Standards Installationspfad";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(361, 20);
            this.textBox1.TabIndex = 3;
            this.textBox1.Text = "C:\\PTC";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(373, 28);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 20);
            this.button2.TabIndex = 4;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // Wizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 350);
            this.Controls.Add(this.HTLStandardsPanel);
            this.Controls.Add(this.CreoPathPanel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.IntroPanel);
            this.Controls.Add(this.panel1);
            this.Name = "Wizard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HTL CREO Einichtungsassistent";
            this.panel1.ResumeLayout(false);
            this.IntroPanel.ResumeLayout(false);
            this.IntroPanel.PerformLayout();
            this.CreoPathPanel.ResumeLayout(false);
            this.CreoPathPanel.PerformLayout();
            this.HTLStandardsPanel.ResumeLayout(false);
            this.HTLStandardsPanel.PerformLayout();
            this.htlStandardsSelect.ResumeLayout(false);
            this.htlStandardsSelect.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button forward;
        private System.Windows.Forms.Panel IntroPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel CreoPathPanel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button creoInstallPathPicker;
        private System.Windows.Forms.TextBox creoInstallPath;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox showOutlinesCheck;
        private System.Windows.Forms.CheckBox betterGraphicsCheck;
        private System.Windows.Forms.CheckBox gpuCheck;
        private System.Windows.Forms.Panel HTLStandardsPanel;
        private System.Windows.Forms.ComboBox htlStandardsPicker;
        private System.Windows.Forms.Panel htlStandardsSelect;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox htlStandardsPath;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
    }
}