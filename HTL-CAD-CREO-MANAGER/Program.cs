﻿using SharedClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HTL_CAD_CREO_MANAGER
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (PreChecks.firstSetupRun())
            {
                Application.Run(new Configurator());
            } else
            {
                Application.Run(new Wizard());
            }
        }
    }
}
