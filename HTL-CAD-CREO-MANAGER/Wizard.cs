﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HTL_CAD_CREO_MANAGER
{
    public partial class Wizard : Form
    {
        private enum WizardStep
        {
            Intro, CreoInstallation, HTLStandardsInstallation, ProjectSetup, End
        }

        private WizardStep currentStep;

        public Wizard()
        {
            InitializeComponent();
            htlStandardsPicker.Items.Add("HTL-Standards sind bereits installiert");
            htlStandardsPicker.Items.Add("HTL-Standards müssen installiert werden");
            // Show the intro step at start
            goToStep(WizardStep.Intro);
        }

        private void goToStep(WizardStep step)
        {
            // Hide all panels first so we don't have to do that in every step
            hideAllPanels();
            switch (step)
            {
                case WizardStep.Intro:
                    IntroPanel.Visible = true;
                    forward.Enabled = true;
                    back.Enabled = false;
                    break;
                case WizardStep.CreoInstallation:
                    CreoPathPanel.Visible = true;
                    forward.Enabled = true;
                    back.Enabled = true;
                    searchCreoPath();
                    break;
                case WizardStep.HTLStandardsInstallation:
                    forward.Enabled = false;
                    back.Enabled = true;
                    HTLStandardsPanel.Visible = true;
                    break;
            }
            currentStep = step;
        }

        private void checkCreoPath()
        {
            if (!SharedClasses.PreChecks.checkCreoPath(@creoInstallPath.Text))
            {
                goToStep(WizardStep.CreoInstallation);
                MessageBox.Show("Der PTC CREO 3 Pfad ist nicht korrekt! Parametric konnte nicht gefunden werden!",
                                   "Falscher Pfad!",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Error);
            }
            else goToNextStep();
        }

        private void searchCreoPath()
        {
            bool creoFound = false;
            if(Directory.Exists(@"C:\Program Files\PTC\Creo 3.0"))
            {
                foreach(var Directory in Directory.GetDirectories(@"C:\Program Files\PTC\Creo 3.0"))
                {
                    var dirName = new DirectoryInfo(Directory).Name;
                    if (dirName.StartsWith("M"))
                    {
                        creoInstallPath.Text = Directory;
                        return;
                    }
                }
                showCreoNotFoundError();
            } else
                showCreoNotFoundError();
        }

        private void showCreoNotFoundError()
        {
            MessageBox.Show("Das PTC CREO 3 Installationsverzeichnis konnte nicht automatisch gefunden werden!\nBitte geben Sie den Pfad manuell in diesem Schritt an.",
                                    "CREO 3 Installation nicht gefunden!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
        }

        private void hideAllPanels()
        {
            IntroPanel.Visible = false;
            CreoPathPanel.Visible = false;
            HTLStandardsPanel.Visible = false;
        }

        // "Zurück" button
        private void backwards_Click(object sender, EventArgs e)
        {
            goToStep(currentStep - 1);
        }

        private void goToNextStep()
        {
            goToStep(currentStep + 1);
        }

        // "Weiter" button
        private void forward_Click(object sender, EventArgs e)
        {
            if (currentStep == WizardStep.End)
            {
                // Do end stuff
            }
            else if (currentStep == WizardStep.CreoInstallation)
            {
                checkCreoPath();
            }
            else
                goToNextStep();
        }

        private void creoInstallPathPicker_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                creoInstallPath.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void gpuCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (gpuCheck.Checked)
                betterGraphicsCheck.Enabled = true;
            else
                betterGraphicsCheck.Enabled = false;
        }

        private void htlStandardsPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            htlStandardsSelect.Visible = false;

            // HTL-Standards already installed
            if(htlStandardsPicker.SelectedIndex == 0)
            {
                htlStandardsSelect.Visible = true;
                string path = Helper.searchForPTCPath();
                if (path == null)
                {
                    MessageBox.Show("Das HTL-Standards Installationsverzeichnis (\"PTC\" Ordner) konnte nicht automatisch gefunden werden!\nBitte geben Sie den Pfad manuell in diesem Schritt an.",
                                    "HTL-Standards nicht gefunden!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                else
                {
                    htlStandardsPath.Text = path;
                    forward.Enabled = true;
                }
            } else
            // HTL-Standards have to be installed
            {

            }
        }
    }
}
