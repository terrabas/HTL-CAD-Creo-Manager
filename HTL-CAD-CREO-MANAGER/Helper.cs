﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTL_CAD_CREO_MANAGER
{
    static class Helper
    {
        public static string searchForPTCPath()
        {
            //looking if PTC folder is present
            if (Directory.Exists(@"C:\PTC\admCreoHTL"))
                return @"C:\PTC";
            else
            {
                //looking for PTC folder on other drives
                string[] drives = System.IO.Directory.GetLogicalDrives();

                foreach (string str in drives)
                {
                    Console.WriteLine(str+@"PTC\");
                    if (Directory.Exists(str + @"PTC\admCreoHTL"))
                        return str + "PTC";
                }
            }
            return null;
        }
    }
}
