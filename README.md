# HTL Creo Manager


#### The HTL Creo Manager is an easy to use tool to deploy and manage the Austrian HTL Standards for PTC Creo 3 on school or student workstations.



A little overview of the features:
  - Automatic download and installation of the HTL-Standards off of a remote server (Students can now install their favourite CAD software at home without a hassle!)
  - Adds support for external GPUs for a more fluent workflow and better performance even with bigger projects.
  - TODO: It's extendable! If you want a BAT script to run before it starts Creo, just add it to the config (For example to add a drive symlink especially for Projects)!
  - TODO: Comes with it's own project management system! Trouble-free working directory selection and project creation!
  - TODO: Since it also supports saving the config to files rather than the registry, students can even run it on school workstations on their own!


The application also includes an easy step-by-step wizard for easy installation and configuration.  



### Building instructions

Just open the Visual Studio solution with VS 2015 or higher, select the "Configurator" or "Launcher" and click "Run. Done! It's that easy!



### The crazy scientist behind this project

[Me - TerraBAS](@terrabas)